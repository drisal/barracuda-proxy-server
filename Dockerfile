FROM java:8
RUN mkdir -p /proxy-server
COPY ./ /proxy-server
WORKDIR /proxy-server
RUN apt-get install -y  wget unzip
RUN wget https://services.gradle.org/distributions/gradle-3.4.1-bin.zip
RUN mkdir /opt/gradle
RUN unzip -d /opt/gradle gradle-3.4.1-bin.zip
ENV PATH="/opt/gradle/gradle-3.4.1/bin:${PATH}"
RUN gradle build
CMD ["gradle", "execute"]
# gRPC Service In Spring, Gradle and Docker: Coding Exercise

> #### DONE:
    1. Implemented Java based gRPC server from the service definition provided in barracuda.proto file.
    2. Implemented the ReadBlob RPC   
    3. Written functional Test that demonstrated a gRPC client connecting to our server and reading file in byte array format
    4. Created Docker file to create image and run in any machine

> #### TODO:
    1. Write Junit Test (Hard to test against gRPC generated code)
    2. (Need to Fix) Azure Service writtent to connect with the azure client and read the file but due to invalid azure connection key, it couldn't connect and unable to read the file.


#### Build  Project Locally
    Clone this repo and build locally
    $ gradle build
        
#### Execute Project Locally
    $ gradle execute


#### Build Docker image
    $ docker build -f Dockerfile -t proxy-server:latest .

#### Run Proxy Server

    $ docker run -p 10000:10000 proxy-server:latest 

###### OR

    This docker image is published to docker hub:  drisal/proxy-server
    So, to start the grpc server to any server running Docker we will just have to do:
        
        $ docker pull drisal/proxy-server
        $ docker run -p 10000:10000 drisal/proxy-server

#### Run Proxy Client to Test gRPC Proxy Server

    1. Open barracuda-proxy-client Project in separate terminal
    2. $ gradle exec ```
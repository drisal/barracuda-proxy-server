// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: barracuda.proto

package com.barracuda.grpc.proto;

public final class BarracudaProto {
  private BarracudaProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ReadBlobRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ReadBlobRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ReadBlobResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ReadBlobResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\017barracuda.proto\"\036\n\017ReadBlobRequest\022\013\n\003" +
      "key\030\001 \001(\t\" \n\020ReadBlobResponse\022\014\n\004data\030\001 " +
      "\001(\0142>\n\tBlobProxy\0221\n\010ReadBlob\022\020.ReadBlobR" +
      "equest\032\021.ReadBlobResponse\"\000B,\n\030com.barra" +
      "cuda.grpc.protoB\016BarracudaProtoP\001b\006proto" +
      "3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_ReadBlobRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_ReadBlobRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ReadBlobRequest_descriptor,
        new java.lang.String[] { "Key", });
    internal_static_ReadBlobResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_ReadBlobResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ReadBlobResponse_descriptor,
        new java.lang.String[] { "Data", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}

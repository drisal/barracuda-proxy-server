package com.barracuda.grpc.proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.22.0)",
    comments = "Source: barracuda.proto")
public final class BlobProxyGrpc {

  private BlobProxyGrpc() {}

  public static final String SERVICE_NAME = "BlobProxy";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.barracuda.grpc.proto.ReadBlobRequest,
      com.barracuda.grpc.proto.ReadBlobResponse> getReadBlobMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ReadBlob",
      requestType = com.barracuda.grpc.proto.ReadBlobRequest.class,
      responseType = com.barracuda.grpc.proto.ReadBlobResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.barracuda.grpc.proto.ReadBlobRequest,
      com.barracuda.grpc.proto.ReadBlobResponse> getReadBlobMethod() {
    io.grpc.MethodDescriptor<com.barracuda.grpc.proto.ReadBlobRequest, com.barracuda.grpc.proto.ReadBlobResponse> getReadBlobMethod;
    if ((getReadBlobMethod = BlobProxyGrpc.getReadBlobMethod) == null) {
      synchronized (BlobProxyGrpc.class) {
        if ((getReadBlobMethod = BlobProxyGrpc.getReadBlobMethod) == null) {
          BlobProxyGrpc.getReadBlobMethod = getReadBlobMethod = 
              io.grpc.MethodDescriptor.<com.barracuda.grpc.proto.ReadBlobRequest, com.barracuda.grpc.proto.ReadBlobResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "BlobProxy", "ReadBlob"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.barracuda.grpc.proto.ReadBlobRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.barracuda.grpc.proto.ReadBlobResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new BlobProxyMethodDescriptorSupplier("ReadBlob"))
                  .build();
          }
        }
     }
     return getReadBlobMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static BlobProxyStub newStub(io.grpc.Channel channel) {
    return new BlobProxyStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static BlobProxyBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new BlobProxyBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static BlobProxyFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new BlobProxyFutureStub(channel);
  }

  /**
   */
  public static abstract class BlobProxyImplBase implements io.grpc.BindableService {

    /**
     */
    public void readBlob(com.barracuda.grpc.proto.ReadBlobRequest request,
        io.grpc.stub.StreamObserver<com.barracuda.grpc.proto.ReadBlobResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getReadBlobMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getReadBlobMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.barracuda.grpc.proto.ReadBlobRequest,
                com.barracuda.grpc.proto.ReadBlobResponse>(
                  this, METHODID_READ_BLOB)))
          .build();
    }
  }

  /**
   */
  public static final class BlobProxyStub extends io.grpc.stub.AbstractStub<BlobProxyStub> {
    private BlobProxyStub(io.grpc.Channel channel) {
      super(channel);
    }

    private BlobProxyStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected BlobProxyStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new BlobProxyStub(channel, callOptions);
    }

    /**
     */
    public void readBlob(com.barracuda.grpc.proto.ReadBlobRequest request,
        io.grpc.stub.StreamObserver<com.barracuda.grpc.proto.ReadBlobResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReadBlobMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class BlobProxyBlockingStub extends io.grpc.stub.AbstractStub<BlobProxyBlockingStub> {
    private BlobProxyBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private BlobProxyBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected BlobProxyBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new BlobProxyBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.barracuda.grpc.proto.ReadBlobResponse readBlob(com.barracuda.grpc.proto.ReadBlobRequest request) {
      return blockingUnaryCall(
          getChannel(), getReadBlobMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class BlobProxyFutureStub extends io.grpc.stub.AbstractStub<BlobProxyFutureStub> {
    private BlobProxyFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private BlobProxyFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected BlobProxyFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new BlobProxyFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.barracuda.grpc.proto.ReadBlobResponse> readBlob(
        com.barracuda.grpc.proto.ReadBlobRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getReadBlobMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_READ_BLOB = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final BlobProxyImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(BlobProxyImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_READ_BLOB:
          serviceImpl.readBlob((com.barracuda.grpc.proto.ReadBlobRequest) request,
              (io.grpc.stub.StreamObserver<com.barracuda.grpc.proto.ReadBlobResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class BlobProxyBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    BlobProxyBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.barracuda.grpc.proto.BarracudaProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("BlobProxy");
    }
  }

  private static final class BlobProxyFileDescriptorSupplier
      extends BlobProxyBaseDescriptorSupplier {
    BlobProxyFileDescriptorSupplier() {}
  }

  private static final class BlobProxyMethodDescriptorSupplier
      extends BlobProxyBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    BlobProxyMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (BlobProxyGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new BlobProxyFileDescriptorSupplier())
              .addMethod(getReadBlobMethod())
              .build();
        }
      }
    }
    return result;
  }
}

package com.barracuda.grpc.proto;

import com.barracuda.grpc.proto.azure.storage.AzureCloudClientService;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlobReadProxyServiceImpl extends BlobProxyGrpc.BlobProxyImplBase implements BlobReadProxyService{

    @Autowired
    AzureCloudClientService azureCloudClientService;

    @Override
    public void readBlob(ReadBlobRequest request,
                         StreamObserver<ReadBlobResponse> responseObserver) {
        byte[] TESTBYTES = "\u00e0\u004f\u00d0\u0020\u00ea\u003a\u0069\u0010\u00a2\u00d8\u0008\u0000\u002b\u0030\u0030\u009d".getBytes();
        ByteString value = azureCloudClientService.getFileFromAzureStorage(request.getKey());
        if(value == null){
            value = ByteString.copyFrom(TESTBYTES);
        }

        ReadBlobResponse response = ReadBlobResponse.newBuilder()
                .setData(value)
                .build();

        // Use responseObserver to send a single response back
        responseObserver.onNext(response);

        // When you are done, you must call onCompleted.
        responseObserver.onCompleted();
    }
}

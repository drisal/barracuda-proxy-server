package com.barracuda.grpc.proto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.TimeZone;

public class ProxyMain {

    private static final Logger logger = LoggerFactory.getLogger(ProxyMain.class);
    private static final int DEFAULT_PORT = 10000;

    public static void main(String[] args) throws IOException, InterruptedException {

        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        //now that we have the configuration files, let the spring context read it and start the message listener container(s).
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:/spring/spring*.xml");
        context.registerShutdownHook();

        int port = getPort(args);
        ProxyServer server = context.getAutowireCapableBeanFactory().createBean(ProxyServer.class);
        server.start(port);
        logger.info("***** Proxy Server Started *****");
    }

    private static int getPort(String[] args) {
        int port = DEFAULT_PORT;
        if (args.length > 0) {
            try {
                String portNum = args[0];
                int parsedPort = Integer.parseInt(portNum);
                if (parsedPort > 0) {
                    port = parsedPort;
                }
            } catch (NumberFormatException ignored) {
            }
        }
        return port;
    }
}

package com.barracuda.grpc.proto.azure.storage;

import com.google.protobuf.ByteString;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

@Service
@Configuration
@PropertySource("classpath:application.properties")
public class AzureCloundClientServiceImpl implements AzureCloudClientService {

    private final Logger logger = LoggerFactory.getLogger(AzureCloundClientServiceImpl.class);

    @Autowired
    private Environment env;

    @Override
    public CloudStorageAccount getCloudStorageAccount() {
        CloudStorageAccount storageAccount = null;
        try {
            storageAccount = CloudStorageAccount.getDevelopmentStorageAccount();
            //CloudStorageAccount.parse(env.getProperty("azure.connection.string"));

//            logger.info(env.getProperty("azure.connection.string"));
        } catch (IllegalArgumentException e) {
            logger.error("Connection string specifies an invalid URI.", e);
            logger.info("Please confirm the connection string is in the Azure connection string format.");
        }
//        catch (InvalidKeyException e) {
//            logger.error("Connection string specifies an invalid key.", e);
//            logger.info("Please confirm the AccountName and AccountKey in the connection string are valid.");
//        }

        return storageAccount;

    }

    @Override
    public ByteString getFileFromAzureStorage(String key) {
//        byte[] fileContent = new byte[0];
        OutputStream outputSteam = new ByteArrayOutputStream();
        try {
            if (getCloudStorageAccount() != null && getCloudStorageAccount().createCloudBlobClient() != null) {
                CloudBlobClient cloudBlobClient = getCloudStorageAccount().createCloudBlobClient();
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.getContainerReference(env.getProperty("azure.container"));
                CloudBlockBlob cloudBlockBlob = null;
                if (cloudBlobContainer != null) {
                    cloudBlockBlob = cloudBlobContainer.getBlockBlobReference(key);
                }

                if (cloudBlockBlob != null) {
                    cloudBlockBlob.download(outputSteam);

                    /*Long fileByteLength = cloudBlockBlob.getProperties().getLength();
                    System.out.println(fileByteLength);
                    int size = 1024*7;
                    fileContent = new byte[size];

                    for (int i = 0; i < size; i++) {
                        fileContent[i] = 0x20;
                    }

                    cloudBlockBlob.downloadToByteArray(fileContent, 0);*/
                }

            }

        } catch (Exception e) {
            logger.error("Not able to read file from azure storage", e);
        }

        return ByteString.copyFrom(((ByteArrayOutputStream) outputSteam).toByteArray());
//        return ByteString.copyFrom((fileContent));
    }
}

package com.barracuda.grpc.proto.azure.storage;

import com.google.protobuf.ByteString;
import com.microsoft.azure.storage.CloudStorageAccount;

public interface AzureCloudClientService {

    /**
     * Validates the connection string and returns the storage account.
     * The connection string must be in the Azure connection string format.
     *
     * @return The newly created CloudStorageAccount object
     */
    CloudStorageAccount getCloudStorageAccount();

    /**
     * By using CloudStorageAccount get the required blob file from Azure Storage.
     *
     **/
    ByteString getFileFromAzureStorage(String key);

}
